import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
public class BibDM{

  /**
  * Ajoute deux entiers
  * @param a le premier entier à ajouter
  * @param b le deuxieme entier à ajouter
  * @return la somme des deux entiers
  */
  public static Integer plus(Integer a, Integer b){
    return a+b;
  }


  /**
  * Renvoie la valeur du plus petit élément d'une liste d'entiers
  * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
  * @param liste
  * @return le plus petit élément de liste
  */
  public static Integer min(List<Integer> liste){
    Integer res = null;
    for (Integer entier: liste) {
      if (res == null || res > entier) {
        res = entier;
      }
    }
    return res;
  }


  /**
  * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
  * @param valeur
  * @param liste
  * @return true si tous les elements de liste sont plus grands que valeur.
  */
  public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    Boolean plusPetit = true;
    int indice = 0;
    while (plusPetit && indice < liste.size()){
      if (liste.get(indice).compareTo(valeur)<=0) plusPetit = false;
      else indice += 1;
    }
    return plusPetit;
  }


  /**
  * Intersection de deux listes données par ordre croissant.
  * @param liste1 une liste triée
  * @param liste2 une liste triée
  * @return une liste triée avec les éléments communs à liste1 et liste2
  */
  public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    List<T> listeRes = new ArrayList<>();
    int indice1 = 0;
    int indice2 = 0;
    while (indice1<liste1.size() && indice2<liste2.size()){
      if (liste1.get(indice1).compareTo(liste2.get(indice2)) == 0){
        if (!listeRes.contains(liste1.get(indice1))){
          listeRes.add(liste1.get(indice1));
        }
        ++indice1;
        ++indice2;
      }
      else if (liste1.get(indice1).compareTo(liste2.get(indice2)) > 1){
        ++indice2;
      }
      else{
        ++indice1;
      }
    }
    return listeRes;
  }


  /**
  * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
  * @param texte une chaine de caractères
  * @return une liste de mots, correspondant aux mots de texte.
  */
  public static List<String> decoupe(String texte){
    List<String> listeMots = new ArrayList<>();
    if (!texte.equals("")){
      String[] parts = texte.split(" ");
      for (int i = 0; i< parts.length; ++i){
        if (!parts[i].equals("")){
          listeMots.add(parts[i]);
        }
      }
    }
    return listeMots;
  }


  /**
  * Renvoie le mot le plus présent dans un texte.
  * @param texte une chaine de caractères
  * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
  */

  public static String motMajoritaire(String texte){
    List<String> listeMots = decoupe(texte);
    Map<String, Integer> dico = new HashMap<>();
    for (String mot: listeMots){
      if (dico.containsKey(mot)){
        dico.put(mot,dico.get(mot)+1);
      }
      else{
        dico.put(mot,1);
      }
    }
    Integer max = null;
    String motRes = null;
    for (String mot: dico.keySet()) {
      if (max == null || max < dico.get(mot)) {
        max = dico.get(mot);
        motRes = mot;
      }
      else if (max == dico.get(mot)){
        if (mot.compareTo(motRes)<0){
          motRes = mot;
        }
      }
    }
    return motRes;
  }

  /**
  * Permet de tester si une chaine est bien parenthesée
  * @param chaine une chaine de caractères composée de ( et de )
  * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
  */
  public static boolean bienParenthesee(String chaine){
    Boolean res = true;
    int indice = 0;
    int cpt = 0;
    while(res && indice < chaine.length()) {
      if (chaine.charAt(indice) == '(') {
        ++cpt;
      }
      else if (chaine.charAt(indice) == ')'){
        --cpt;
      }
      if (cpt < 0) {
        res = false;
      }
      ++indice;
    }
    if (cpt != 0) res = false;
    return res;
  }

  /**
  * Permet de tester si une chaine est bien parenthesée
  * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
  * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
  */
  public static boolean bienParentheseeCrochets(String chaine){
    Boolean res = true;
    if (chaine.length()!=0) {
      int indice = 0;
      List<Character> liste = new ArrayList<>();
      while (res && indice<chaine.length()) {
        if (chaine.charAt(indice)=='(' || chaine.charAt(indice)=='[') {
          liste.add(chaine.charAt(indice));
        }
        else{
          if (liste.size()==0) {
            res = false;
          }
          else{
            if ((chaine.charAt(indice) == ')' && liste.get(liste.size()-1) == '(') || (chaine.charAt(indice) == ']' && liste.get(liste.size()-1) == '[')) {
              liste.remove(liste.size()-1);
            }
            else {
              res = false;
            }
          }
        }
        ++indice;
      }
    }
    return res;
  }

  /**
  * echerche par dichtomie d'un élément dans une liste triée
  * @param liste une liste triée d'entiers
  * @param valeur un entier
  * @return true si l'entier appartient à la liste.
  */
  public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    boolean res = false;
    if(liste.size() != 0){
      int indDebut = 0;
      int indFin = liste.size()-1;
      int indMil;
      while (!res && (indFin-indDebut)>=1){
        indMil = (indDebut+indFin)/2;
        res = (liste.get(indMil)==valeur);
        if (liste.get(indMil) > valeur) indFin = indMil;
        else indDebut = indMil;
      }
    }
    return res;
  }



}
